# Dataset link

https://bayanat.ae/en/Data?themeid=49

# Imports


```python
import pandas as pd
import matplotlib.pyplot as plt
```

# Read Data


```python
df = pd.read_excel("pod_card_db (1).xlsx")
```

# Data Info


```python
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 3508 entries, 0 to 3507
    Data columns (total 8 columns):
     #   Column                 Non-Null Count  Dtype  
    ---  ------                 --------------  -----  
     0   Gender                 3508 non-null   object 
     1   Age Groups             3508 non-null   int64  
     2   Year of birth          3508 non-null   int64  
     3   type of disability     3508 non-null   object 
     4   Emirate                3508 non-null   object 
     5   Citizens-Non-Citizens  3508 non-null   object 
     6   nationality            3508 non-null   object 
     7   Issue Date             3508 non-null   float64
    dtypes: float64(1), int64(2), object(5)
    memory usage: 219.4+ KB
    


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Gender</th>
      <th>Age Groups</th>
      <th>Year of birth</th>
      <th>type of disability</th>
      <th>Emirate</th>
      <th>Citizens-Non-Citizens</th>
      <th>nationality</th>
      <th>Issue Date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ذكر</td>
      <td>7</td>
      <td>2009</td>
      <td>ذهنية</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
      <td>42689.408906</td>
    </tr>
    <tr>
      <th>1</th>
      <td>أنثى</td>
      <td>8</td>
      <td>2008</td>
      <td>توحد</td>
      <td>الشارقة</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
      <td>42437.563619</td>
    </tr>
    <tr>
      <th>2</th>
      <td>أنثى</td>
      <td>4</td>
      <td>2012</td>
      <td>جسدية</td>
      <td>أم القيوين</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
      <td>42572.524340</td>
    </tr>
    <tr>
      <th>3</th>
      <td>ذكر</td>
      <td>4</td>
      <td>2012</td>
      <td>جسدية</td>
      <td>عجمان</td>
      <td>غير مواطن</td>
      <td>مصر</td>
      <td>42675.358322</td>
    </tr>
    <tr>
      <th>4</th>
      <td>ذكر</td>
      <td>4</td>
      <td>2012</td>
      <td>متعددة</td>
      <td>رأس الخيمة</td>
      <td>غير مواطن</td>
      <td>جزر القمر</td>
      <td>42631.393873</td>
    </tr>
  </tbody>
</table>
</div>




```python
df.sample(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Gender</th>
      <th>Age Groups</th>
      <th>Year of birth</th>
      <th>type of disability</th>
      <th>Emirate</th>
      <th>Citizens-Non-Citizens</th>
      <th>nationality</th>
      <th>Issue Date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3141</th>
      <td>ذكر</td>
      <td>5</td>
      <td>2011</td>
      <td>توحد</td>
      <td>أبوظبي</td>
      <td>غير مواطن</td>
      <td>سوريا</td>
      <td>42690.406885</td>
    </tr>
    <tr>
      <th>2735</th>
      <td>أنثى</td>
      <td>5</td>
      <td>2011</td>
      <td>متعددة</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
      <td>42633.448933</td>
    </tr>
    <tr>
      <th>2817</th>
      <td>أنثى</td>
      <td>19</td>
      <td>1997</td>
      <td>سمعية</td>
      <td>دبي</td>
      <td>غير مواطن</td>
      <td>مصر</td>
      <td>42633.353704</td>
    </tr>
    <tr>
      <th>124</th>
      <td>ذكر</td>
      <td>3</td>
      <td>2013</td>
      <td>جسدية</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
      <td>42535.526509</td>
    </tr>
    <tr>
      <th>2131</th>
      <td>أنثى</td>
      <td>9</td>
      <td>2007</td>
      <td>ذهنية</td>
      <td>الفجيرة</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
      <td>42529.432836</td>
    </tr>
  </tbody>
</table>
</div>



# Data Cleaning


```python
df2 = df
```

### Drop Unuseful Columns


```python
df2 = df.drop('Issue Date', axis=1)
```

### Handle Missing Values


```python
df2.isnull().sum().max() # No missing values
```




    0



### Duplications


```python
df2.duplicated().sum()
```




    913




```python
df2[df2.duplicated()]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Gender</th>
      <th>Age Groups</th>
      <th>Year of birth</th>
      <th>type of disability</th>
      <th>Emirate</th>
      <th>Citizens-Non-Citizens</th>
      <th>nationality</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>80</th>
      <td>أنثى</td>
      <td>19</td>
      <td>1997</td>
      <td>ذهنية</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>95</th>
      <td>أنثى</td>
      <td>4</td>
      <td>2012</td>
      <td>ذهنية</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>109</th>
      <td>ذكر</td>
      <td>6</td>
      <td>2010</td>
      <td>توحد</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>113</th>
      <td>ذكر</td>
      <td>27</td>
      <td>1989</td>
      <td>ذهنية</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>125</th>
      <td>ذكر</td>
      <td>11</td>
      <td>2005</td>
      <td>ذهنية</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>3501</th>
      <td>ذكر</td>
      <td>4</td>
      <td>2012</td>
      <td>توحد</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>3503</th>
      <td>أنثى</td>
      <td>12</td>
      <td>2004</td>
      <td>ذهنية</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>3505</th>
      <td>ذكر</td>
      <td>42</td>
      <td>1974</td>
      <td>جسدية</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>3506</th>
      <td>ذكر</td>
      <td>4</td>
      <td>2012</td>
      <td>توحد</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>3507</th>
      <td>أنثى</td>
      <td>11</td>
      <td>2005</td>
      <td>متعددة</td>
      <td>أبوظبي</td>
      <td>مواطن</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
  </tbody>
</table>
<p>913 rows × 7 columns</p>
</div>



### Rename Columns


```python
df2.columns
```




    Index(['Gender', 'Age Groups', 'Year of birth', 'type of disability',
           'Emirate', 'Citizens-Non-Citizens', 'nationality'],
          dtype='object')




```python
df2.columns = ['Gender','Age','Birth','Disbility','Emirate','Citizenship_Status','Nationality']
```


```python
df.Gender.unique()[0]
```




    'ذكر'



### Change Values to English


```python
df2.Gender = df2.Gender.apply(lambda x: 'Male' if x == 'ذكر'  else 'Female')
```


```python
df2.Citizenship_Status.unique()
```




    array(['مواطن', 'غير مواطن'], dtype=object)




```python
df2.Citizenship_Status = df2.Citizenship_Status.apply(lambda x: 'Citizen' if x == 'مواطن' else 'Non-Citizen')
```


```python
df2.Disbility.unique()
```




    array(['ذهنية', 'توحد', 'جسدية', 'متعددة', 'سمعية', 'بصرية'], dtype=object)




```python
df2.Emirate.unique()
```




    array(['أبوظبي', 'الشارقة', 'أم القيوين', 'عجمان', 'رأس الخيمة', 'دبي',
           'الفجيرة'], dtype=object)




```python
len(df2.Nationality.unique())
```




    67




```python
def changeEmirateToEnglish(emirate):
    if emirate == "دبي":
        return "Dubai"
    elif emirate == "عجمان":
        return "Ajman"
    elif emirate == "رأس الخيمة":
        return "Ras Alkhaima"
    elif emirate == "أم القيوين":
        return "Um ALqiaween"
    elif emirate == "أبوظبي":
        return "Abu Dabi"
    elif emirate == "الفجيرة":
        return "Al Fujaera"
    elif emirate == "الشارقة":
        return "Al Shariqa"
```


```python
df2.Emirate = df2.Emirate.apply(changeEmirateToEnglish)
```


```python
df2.Emirate.unique()
```




    array(['Abu Dabi', 'Al Shariqa', 'Um ALqiaween', 'Ajman', 'Ras Alkhaima',
           'Dubai', 'Al Fujaera'], dtype=object)




```python
df2.Age.describe()
```




    count    3508.000000
    mean       21.220924
    std        17.354534
    min         0.000000
    25%         7.000000
    50%        16.000000
    75%        32.000000
    max        86.000000
    Name: Age, dtype: float64




```python
df = df2
```

# Statistics

### What is the average ages of disabled people in the UAE?


```python
df.Age.mean()
```




    21.220923603192702



### What is the average of the ages of female disabled people in the UAE?


```python
len(df[df.Gender == 'Female'])
```




    1276



### What is the number of deff cases of females?


```python
len(df[(df.Gender == 'Female') & (df.Disbility == 'سمعية')])
```




    125



### What is the number of disabled Citezen who have been born between the years 2000 and 2005?


```python
len(df[(df.Birth > 2000) & (df.Birth < 2005) & (df.Citizenship_Status == 'Citizen')])
```




    151



### What is the average age of the disabled people in each Emirate?


```python
df.groupby("Emirate").Age.mean()
```




    Emirate
    Abu Dabi        19.610272
    Ajman           20.851613
    Al Fujaera      20.562500
    Al Shariqa      23.810552
    Dubai           19.842960
    Ras Alkhaima    24.032000
    Um ALqiaween    23.370370
    Name: Age, dtype: float64



### What is the number of disabled people in each Emirate sorted ascending?


```python
df.Emirate.value_counts().sort_values()
```




    Um ALqiaween     108
    Al Fujaera       128
    Ras Alkhaima     250
    Ajman            310
    Dubai            554
    Al Shariqa       834
    Abu Dabi        1324
    Name: Emirate, dtype: int64



### What are the most frequent ages for the disabled people in "Abu Dabi" emirate?


```python
plt.figure(figsize=[8,6])
df[df.Emirate == "Abu Dabi"].Age.value_counts().sort_index().plot.line()
```




    <AxesSubplot:>




    
![png](images/output_48_1.png)
    


### What is the disabilities ratio for the citizens in each emirate?


```python
df[df.Citizenship_Status =="Citizen"].Emirate.value_counts().sort_index().plot.barh()
```




    <AxesSubplot:>




    
![png](images/output_50_1.png)
    


### What is the disabilities ration for the non-citizens in each emirate?


```python
df[df.Citizenship_Status =="Non-Citizen"].Emirate.value_counts().sort_index().plot.barh()
```




    <AxesSubplot:>




    
![png](images/output_52_1.png)
    


### What is the ratio between citizen and non-citizen disabled people in the UAE?


```python
df.Citizenship_Status.value_counts().plot.bar()
```




    <AxesSubplot:>




    
![png](images/output_54_1.png)
    





```python
df[df.Disbility == 'بصرية'].Emirate.value_counts().plot.barh()
```




    <AxesSubplot:>




    
![png](images/output_56_1.png)
    



```python
df.Nationality.values[0]
```




    ' \u200fالامارات العربية المتحدة'




```python
df[df.Nationality == " \u200fالامارات العربية المتحدة"].Disbility.value_counts().plot.pie()
```




    <AxesSubplot:ylabel='Disbility'>




    
![png](images/output_58_1.png)
    



```python
df[df.Gender =="Male"].Emirate.value_counts().sort_index().plot.barh()
```




    <AxesSubplot:>




    
![png](images/output_59_1.png)
    



```python
df[df.Gender =="Female"].Emirate.value_counts().sort_index().plot.barh()
```




    <AxesSubplot:>




    
![png](images/output_60_1.png)
    



```python
df.Gender.value_counts().plot.bar()
```




    <AxesSubplot:>




    
![png](images/output_61_1.png)
    



```python
df[df.Disbility == 'توحد'].Gender.value_counts().plot.pie()
```




    <AxesSubplot:ylabel='Gender'>




    
![png](images/output_62_1.png)
    



```python
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 3508 entries, 0 to 3507
    Data columns (total 7 columns):
     #   Column              Non-Null Count  Dtype 
    ---  ------              --------------  ----- 
     0   Gender              3508 non-null   object
     1   Age                 3508 non-null   int64 
     2   Birth               3508 non-null   int64 
     3   Disbility           3508 non-null   object
     4   Emirate             3508 non-null   object
     5   Citizenship_Status  3508 non-null   object
     6   Nationality         3508 non-null   object
    dtypes: int64(2), object(5)
    memory usage: 192.0+ KB
    


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Gender</th>
      <th>Age</th>
      <th>Birth</th>
      <th>Disbility</th>
      <th>Emirate</th>
      <th>Citizenship_Status</th>
      <th>Nationality</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Male</td>
      <td>7</td>
      <td>2009</td>
      <td>ذهنية</td>
      <td>Abu Dabi</td>
      <td>Citizen</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Female</td>
      <td>8</td>
      <td>2008</td>
      <td>توحد</td>
      <td>Al Shariqa</td>
      <td>Citizen</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Female</td>
      <td>4</td>
      <td>2012</td>
      <td>جسدية</td>
      <td>Um ALqiaween</td>
      <td>Citizen</td>
      <td>‏الامارات العربية المتحدة</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Male</td>
      <td>4</td>
      <td>2012</td>
      <td>جسدية</td>
      <td>Ajman</td>
      <td>Non-Citizen</td>
      <td>مصر</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Male</td>
      <td>4</td>
      <td>2012</td>
      <td>متعددة</td>
      <td>Ras Alkhaima</td>
      <td>Non-Citizen</td>
      <td>جزر القمر</td>
    </tr>
  </tbody>
</table>
</div>


